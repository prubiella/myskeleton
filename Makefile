.PHONY: build dc-build shell shell-nginx start stop down logs

IMAGE_NAME := myskeleton

build:
	@docker-compose build 

shell:
	@docker-compose exec fpm /bin/bash

start: 
	@docker-compose up -d --remove-orphans

stop: 
	@docker-compose stop

down:

	@docker-compose down 

logs:
	@docker-compose logs -f fpm